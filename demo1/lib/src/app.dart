import 'package:demo1/src/bloc/counter/counter_bloc.dart';
import 'package:demo1/src/bloc/login/login_bloc.dart';
import 'package:demo1/src/pages/app_routes.dart';
import 'package:demo1/src/pages/home/home_page.dart';
import 'package:demo1/src/pages/login/login_page.dart';
import 'package:demo1/src/pages/register/register_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class App extends StatelessWidget {
  App({super.key});

  final counterBloc = BlocProvider(create: (context) => CounterBloc());
  final loginBloc = BlocProvider(create: (context) => LoginBloc());

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        counterBloc,
        loginBloc,
      ],
      child: MaterialApp(
        title: "Demo1",
        routes: AppRoute.all,
        home: LoginPage(),
      ),
    );
  }
}
