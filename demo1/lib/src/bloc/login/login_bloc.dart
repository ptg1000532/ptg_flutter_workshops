import 'package:bloc/bloc.dart';
import 'package:demo1/src/models/user.dart';
import 'package:equatable/equatable.dart';

part 'login_event.dart';
part 'login_state.dart';

class LoginBloc extends Bloc<LoginEvent, LoginState> {
  LoginBloc()
      : super(LoginState(
          status: LoginStatus.init,
          dialogMessage: "",
        )) {
    // Login
    on<LoginEventLogin>((event, emit) {
      print("Debug: Login: " + event.payload.toString());
    });

    // Register
    on<LoginEventRegister>((event, emit) {
      print("Debug: Register: " + event.payload.toString());
    });
  }
}
