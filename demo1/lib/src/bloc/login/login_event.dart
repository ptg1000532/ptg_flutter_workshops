part of 'login_bloc.dart';

sealed class LoginEvent {
  const LoginEvent();
}

class LoginEventLogin extends LoginEvent {
  final User payload;

  LoginEventLogin(this.payload);
}

class LoginEventRegister extends LoginEvent {
  final User payload;

  LoginEventRegister(this.payload);
}
