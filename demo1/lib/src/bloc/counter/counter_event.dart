part of 'counter_bloc.dart';

sealed class CounterEvent {
  const CounterEvent();
}

class CounterEventAdd extends CounterEvent {}

class CounterEventRemove extends CounterEvent {}
