import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

part 'counter_event.dart';
part 'counter_state.dart';

class CounterBloc extends Bloc<CounterEvent, CounterState> {
  CounterBloc() : super(CounterState(count: 10)) {
    // Add
    on<CounterEventAdd>((event, emit) {
      emit(CounterState(count: state.count + 1));
    });

    // Remove
    on<CounterEventRemove>((event, emit) {
      emit(CounterState(count: state.count - 1));
    });
  }
}
