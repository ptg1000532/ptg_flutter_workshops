// ignore_for_file: prefer_const_literals_to_create_immutables, prefer_const_constructors

import 'package:demo1/src/bloc/counter/counter_bloc.dart';
import 'package:demo1/src/bloc/login/login_bloc.dart';
import 'package:demo1/src/constants/asset.dart';
import 'package:demo1/src/models/user.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({super.key});

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final _usernameController = TextEditingController();
  final _passwordController = TextEditingController();

  @override
  void initState() {
    super.initState();
    _usernameController.text = "admin";
    _passwordController.text = "1234";
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        padding: const EdgeInsets.only(top: 70),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            // Header
            SizedBox(height: 100, child: Image.asset(Asset.logoImage)),
            // Form
            _buildForm(),
            // Counter
            _buildCounter()
          ],
        ),
      ),
    );
  }

  _buildForm() {
    return Padding(
      padding: const EdgeInsets.all(16.0),
      child: Card(
        elevation: 7,
        child: Padding(
          padding: EdgeInsets.all(16.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              // Username
              TextField(
                  controller: _usernameController,
                  decoration: InputDecoration(
                    border: InputBorder.none,
                    hintText: 'codemobiles@gmail.com',
                    labelText: 'Username',
                    icon: Icon(Icons.email),
                  )),

              // Password
              TextField(
                  controller: _passwordController,
                  obscureText: true,
                  decoration: InputDecoration(
                    border: InputBorder.none,
                    hintText: '',
                    labelText: 'Password',
                    icon: Icon(Icons.lock),
                  )),

              // Login Button
              ElevatedButton(onPressed: _login, child: Text("Login")),
              // Register Button
              OutlinedButton(onPressed: _register, child: Text("Register"))
            ],
          ),
        ),
      ),
    );
  }

  void _login() {
    // Navigator.pushNamed(context, "home");
    final user = User(
      _usernameController.text,
      _passwordController.text,
    );

    context.read<LoginBloc>().add(LoginEventLogin(user));
  }

  void _register() {
    // Navigator.pushNamed(context, "register");

    final user = User(
      _usernameController.text,
      _passwordController.text,
    );

    context.read<LoginBloc>().add(LoginEventRegister(user));
  }

  _buildCounter() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        //  Minus button
        TextButton(
          onPressed: () {
            context.read<CounterBloc>().add(CounterEventRemove());
          },
          child: Text(
            "-",
            style: TextStyle(fontSize: 30),
          ),
        ),

        // Count
        BlocBuilder<CounterBloc, CounterState>(
          builder: (context, state) {
            return Text(
              "${state.count}",
              style: TextStyle(fontSize: 30),
            );
          },
        ),

        // Add Button
        TextButton(
          onPressed: () {
            context.read<CounterBloc>().add(CounterEventAdd());
          },
          child: Text(
            "+",
            style: TextStyle(fontSize: 30),
          ),
        )
      ],
    );
  }
}
