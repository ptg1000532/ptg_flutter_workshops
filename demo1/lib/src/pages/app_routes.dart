import 'package:demo1/src/pages/pages.dart';
import 'package:demo1/src/pages/register/register_page.dart';
import 'package:demo1/src/pages/sqlite/sqlite_page.dart';
import 'package:flutter/material.dart';

class AppRoute {
  static const home = 'home';
  static const login = 'login';
  static const register = 'register';
  static const management = 'management';
  static const map = 'map';
  static const loading = 'loading';

  // Optional Pages
  static const sqlite = 'sqlite';
  static const firebase_push = 'firebase_push';
  static const firebase_auth = 'firebase_auth';
  static const forget_password = 'forget_password';
  static const firebase_store = 'firebase_store';
  static const firebase_analytics = 'firebase_analytics';

  static get all => <String, WidgetBuilder>{
        login: (context) => const LoginPage(),
        register: (context) => const RegisterPage(),
        home: (context) => const HomePage(),
        management: (context) => const ManagementPage(),
        map: (context) => const MapPage(),
        loading: (context) => const LoadingPage(),
        sqlite: (context) => const SqlitePage(),
      };
}
